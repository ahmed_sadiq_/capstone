//
//  CompanyNewsVC.h
//  Capstone
//
//  Created by Ahmed Sadiq on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyNewsVC : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mainTblView;
@end
