//
//  CompanyNewsVC.m
//  Capstone
//
//  Created by Ahmed Sadiq on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CompanyNewsVC.h"
#import "NewsAndEventCell.h"
@interface CompanyNewsVC ()

@end

@implementation CompanyNewsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NewsAndEventCell * cell = (NewsAndEventCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"NewsAndEventCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"NewsAndEventCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    [cell.readBtn addTarget:self
               action:@selector(readMorePressed:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


- (void)readMorePressed :(id) sender {
    [self performSegueWithIdentifier:@"newsDetailVC" sender:self];
}



@end
