//
//  CompanyProfileVC.h
//  Capstone
//
//  Created by Ahmed Sadiq on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyProfileVC : UIViewController {
    
    IBOutlet UIButton *introBtn;
    IBOutlet UIButton *missionBtn;
    IBOutlet UIButton *clientBtn;
}

@property (strong, nonatomic) IBOutlet UIView *introductionView;
@property (strong, nonatomic) IBOutlet UIView *missionView;
@property (strong, nonatomic) IBOutlet UIView *clientView;
- (IBAction)introductionPressed:(id)sender;
- (IBAction)missionPressed:(id)sender;
- (IBAction)clientPressed:(id)sender;
@end
