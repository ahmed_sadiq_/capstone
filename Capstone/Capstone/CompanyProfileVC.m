//
//  CompanyProfileVC.m
//  Capstone
//
//  Created by Ahmed Sadiq on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CompanyProfileVC.h"

@interface CompanyProfileVC ()

@end

@implementation CompanyProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)introductionPressed:(id)sender {
    _introductionView.hidden = false;
    _missionView.hidden = true;
    _clientView.hidden = true;
    
    [introBtn setBackgroundImage:[UIImage imageNamed:@"careers"] forState:UIControlStateNormal];
    [missionBtn setBackgroundImage:[UIImage imageNamed:@"active_txt_bg"] forState:UIControlStateNormal];
    [clientBtn setBackgroundImage:[UIImage imageNamed:@"active_txt_bg"] forState:UIControlStateNormal];
    
    [introBtn.titleLabel setTextColor:[UIColor whiteColor]];
    //[missionBtn.titleLabel setTextColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0]];
    //[clientBtn.titleLabel setTextColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0]];
    
    
    [introBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [missionBtn setTitleColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0] forState:UIControlStateNormal];
    [clientBtn setTitleColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0] forState:UIControlStateNormal];
}

- (IBAction)missionPressed:(id)sender {
    _introductionView.hidden = true;
    _missionView.hidden = false;
    _clientView.hidden = true;
    
    [introBtn setBackgroundImage:[UIImage imageNamed:@"active_txt_bg"] forState:UIControlStateNormal];
    [missionBtn setBackgroundImage:[UIImage imageNamed:@"careers"] forState:UIControlStateNormal];
    [clientBtn setBackgroundImage:[UIImage imageNamed:@"active_txt_bg"] forState:UIControlStateNormal];
    
    [missionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [introBtn setTitleColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0] forState:UIControlStateNormal];
    [clientBtn setTitleColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0] forState:UIControlStateNormal];
}

- (IBAction)clientPressed:(id)sender {
    _introductionView.hidden = true;
    _missionView.hidden = true;
    _clientView.hidden = false;
    
    [introBtn setBackgroundImage:[UIImage imageNamed:@"active_txt_bg"] forState:UIControlStateNormal];
    [missionBtn setBackgroundImage:[UIImage imageNamed:@"active_txt_bg"] forState:UIControlStateNormal];
    [clientBtn setBackgroundImage:[UIImage imageNamed:@"careers"] forState:UIControlStateNormal];
    
    [clientBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [missionBtn setTitleColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0] forState:UIControlStateNormal];
    [introBtn setTitleColor:[UIColor colorWithRed:0.313 green:0.094 blue:0.207 alpha:1.0] forState:UIControlStateNormal];
}
@end
