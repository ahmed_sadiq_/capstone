//
//  LandingScreenVC.h
//  Capstone
//
//  Created by Ahmed Sadiq on 11/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "EMCCountry.h"
#import "EMCCountryPickerController.h"



@interface LandingScreenVC : UIViewController<EMCCountryDelegate,NIDropDownDelegate> {
    EMCCountry *selectedCountry;
    BOOL isSliderShown;
    NIDropDown *dropDown;
    IBOutlet UIScrollView *mainScroll;
}
@property (strong, nonatomic) IBOutlet UIButton *forRentBtn;
@property (strong, nonatomic) IBOutlet UIButton *forSaleBtn;

@property (strong, nonatomic) IBOutlet UIButton *typeOfPropertyBtn;
@property (strong, nonatomic) IBOutlet UIButton *bedRoomBtn;

@property (strong, nonatomic) IBOutlet UIView *sliderView;
@property (strong, nonatomic) IBOutlet UIButton *countryBtn;
- (IBAction)countryBtnPressed:(id)sender;
- (IBAction)propertyTypePressed:(id)sender;
- (IBAction)numOfBedroomBtn:(id)sender;


- (IBAction)forSalePressed:(id)sender;
- (IBAction)forRentPressed:(id)sender;

@end
