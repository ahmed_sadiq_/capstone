//
//  LandingScreenVC.m
//  Capstone
//
//  Created by Ahmed Sadiq on 11/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "LandingScreenVC.h"
#import "RangeSlider.h"

@interface LandingScreenVC ()

@end

@implementation LandingScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initData];
    
}

- (void) initData {
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSString *country = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
    
    [self.countryBtn setTitle:country forState:UIControlStateNormal];
    selectedCountry = [[EMCCountry alloc] initWithCountryCode:@"AU"];
    
    if(!isSliderShown) {
        RangeSlider *slider=  [[RangeSlider alloc] initWithFrame:self.sliderView.bounds];
        slider.minimumValue = 0;
        slider.selectedMinimumValue = 0;
        slider.maximumValue = 10;
        slider.selectedMaximumValue = 10;
        slider.minimumRange = 2;
        [slider addTarget:self action:@selector(updateRangeLabel:) forControlEvents:UIControlEventValueChanged];
        
        
        [self.sliderView addSubview:slider];
        
        isSliderShown = true;
    }
    
    mainScroll.contentSize = CGSizeMake(0, 650);
    
}

-(void)updateRangeLabel:(RangeSlider *)slider{
    NSLog(@"Slider Range: %f - %f", slider.selectedMinimumValue, slider.selectedMaximumValue);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity
{
    [self.countryBtn setTitle:chosenCity.countryName forState:UIControlStateNormal];
    selectedCountry = chosenCity;
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"openCountryPicker"])
    {
        EMCCountryPickerController *countryPicker = segue.destinationViewController;
        countryPicker.showFlags = true;
        countryPicker.countryDelegate = self;
        countryPicker.drawFlagBorder = true;
        countryPicker.flagBorderColor = [UIColor grayColor];
        countryPicker.flagBorderWidth = 0.5f;
        countryPicker.flagSize = 30;
    }
}


- (IBAction)countryBtnPressed:(id)sender {
    
}

- (IBAction)propertyTypePressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects: @"Commerical", @"Residence", @"Mall", @"Shop", @"Garden", @"Hotel",nil];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
        dropDown.tag = 1;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

#pragma mark -
#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}
- (IBAction)numOfBedroomBtn:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects: @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10",nil];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
        dropDown.tag = 2;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)forSalePressed:(id)sender {
    
    [_forSaleBtn setBackgroundImage:[UIImage imageNamed:@"radiobtn_a"] forState:UIControlStateNormal];
    [_forRentBtn setBackgroundImage:[UIImage imageNamed:@"radiobtn"] forState:UIControlStateNormal];
}

- (IBAction)forRentPressed:(id)sender {
    [_forSaleBtn setBackgroundImage:[UIImage imageNamed:@"radiobtn"] forState:UIControlStateNormal];
    [_forRentBtn setBackgroundImage:[UIImage imageNamed:@"radiobtn_a"] forState:UIControlStateNormal];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    if(sender.tag == 1) {
        NSLog(@"%@", _typeOfPropertyBtn.titleLabel.text);
    }
    else {
        NSLog(@"%@", _bedRoomBtn.titleLabel.text);
    }
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}
@end
