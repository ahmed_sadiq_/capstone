//
//  NewsAndEventCell.h
//  Capstone
//
//  Created by Ahmed Sadiq on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsAndEventCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *readBtn;
@end
