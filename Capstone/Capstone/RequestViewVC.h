//
//  RequestViewVC.h
//  Capstone
//
//  Created by Ahmed Sadiq on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestViewVC : UIViewController<UITextFieldDelegate>
@property float animatedDistance;
@end
