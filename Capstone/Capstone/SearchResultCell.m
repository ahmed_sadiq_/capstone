//
//  SearchResultCell.m
//  Capstone
//
//  Created by Ahmed Sadiq on 12/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "SearchResultCell.h"

@implementation SearchResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
