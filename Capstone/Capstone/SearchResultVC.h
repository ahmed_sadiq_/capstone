//
//  SearchResultVC.h
//  Capstone
//
//  Created by Ahmed Sadiq on 12/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *mainTblView;

@end
