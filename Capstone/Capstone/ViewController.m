//
//  ViewController.m
//  Capstone
//
//  Created by Ahmed Sadiq on 11/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationController.interactivePopGestureRecognizer setDelegate:nil];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
